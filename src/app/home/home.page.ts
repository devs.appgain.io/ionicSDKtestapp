import { Component } from '@angular/core';
import { AppgainPlugin } from '@ionic-native/appgain-plugin';
import { AlertController, LoadingController } from '@ionic/angular';
//import { resolve } from 'dns';
import { rejects } from 'assert';
//import { resolve } from 'dns';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  loading
  constructor(public alertController: AlertController,
    public loadingCtrl: LoadingController,
  ) { }
  // async initSDK() {
  //   try {
  //     await  AppgainPlugin.initSDK(
  //           "5fd6228e9c2095000dd2f042",
  //           "d766e0fa909ec05af75c14594e8f90540123c547de53dadc2e6d11de7fb22a1b", 
  //           true
  //       ).then(
  //         result => this.showAlert(result),

  //         error => alert(error) // doesn't run
  //       )


  //      // this.showAlert
  //   } catch (error) {
  //       console.log("Error: ", error);
  //   }
  // }






  async show() {
    this.loading = await this.loadingCtrl.create({
      // duration: 1000,
    });
    this.loading.present();
  }
  hide() {
    try {
      this.loading.dismiss();
    } catch (error) { }
  }

  async initSDK() {

    let resolveFunction: (confirm: any) => void;
    const promise = new Promise<any>(resolve => {
      resolveFunction = resolve;
    });
    let prompt = await this.alertController.create({
      header: 'Init Sdk',
      inputs: [
        {
          name: 'appId',
          placeholder: 'App id',
          value: '6061bc9963b59b000922a5f2',
        },
        {
          name: 'apiKey',
          placeholder: 'Api key',
          value: 'd297893115b8d00a488e65d342f4d74021f6a05963779c349560d87a0575f188'
        },

      ],
      buttons: [
        { text: 'Cancel' },
        {
          text: 'OK',
          handler: async function (data: any): Promise<void> {
            return resolveFunction(data)

          }
        }
      ]
    });
    await prompt.present();
    promise.then(
      data => this.initalizeSDK(data['appId'], data['apiKey']),
      error => alert(error) // doesn't run
    )
  }
  //"5fd6228e9c2095000dd2f042"
  //"d766e0fa909ec05af75c14594e8f90540123c547de53dadc2e6d11de7fb22a1b"
  async initalizeSDK(appId: string, apiKey: string) {
    await this.show();
    await AppgainPlugin.initSDK(
      appId,
      apiKey,
      true
    ).then(
      result => {
        this.hide();
        this.showAlert(result)
      },
      error => {
        this.hide();
        alert(error)
      }// doesn't run
    )
  }





  async getuserId() {
    try {
      await this.show();
      await AppgainPlugin.getUserId().then(
        (result: any) => {
          console.log(result)
          this.hide();
          this.showAlertt('userId= ' + result.userId)
        },
        error => {
          this.hide();
          alert(error)
        }// doesn't run
      )
      // this.showAlert
    } catch (error) {
      this.hide();
      console.log("Error: ", error);
    }
  }
  async matchLink() {
    try {
      await this.show();

      await AppgainPlugin.matchLink().then(
        result => {
          this.hide();
          this.showAlert(result)
        },
        error => {
          this.hide();
          alert(error)
        }// doesn't run
      )
      // this.showAlert
    } catch (error) {
      this.hide();

      console.log("Error: ", error);
    }
  }

  async fireAutomatorWithPers() {
    let resolveFunction: (confirm: any) => void;
    const promise = new Promise<any>(resolve => {
      resolveFunction = resolve;
    });
    let prompt = await this.alertController.create({
      header: 'triggeer with parameter',
      inputs: [
        {
          name: 'triggerPoint',
          placeholder: 'trigger point name'
        },
        {
          name: 'key',
          placeholder: 'name'
        },
        {
          name: 'value',
          placeholder: 'value'
        },
      ],
      buttons: [
        { text: 'Cancel' },
        {
          text: 'OK',
          handler: async function (data: any): Promise<any> {
            return resolveFunction(data)
          }
        }
      ]
    });
    await prompt.present();
    promise.then(
      result => this.fireAutomatorff(result),
      error => alert(error) // doesn't run
    )
  }

  async fireAutomatorff(data: any) {
    await this.show();

    var dict = {};
    dict[data['key']] = data['value'];

    await AppgainPlugin.fireAutomator(data['triggerPoint'], dict).then(
      result => {
        this.hide();
        this.showAlert(result)
      },
      error => {
        this.hide();
        alert(error)
      } // doesn't run
    )
  }

  async fireAutomator() {
    let resolveFunction: (confirm: any) => void;
    const promise = new Promise<any>(resolve => {
      resolveFunction = resolve;
    });
    let prompt = await this.alertController.create({
      header: 'triggeer with parameter',
      inputs: [
        {
          name: 'triggerPoint',
          placeholder: 'trigger point name'
        },

      ],
      buttons: [
        { text: 'Cancel' },

        {
          text: 'OK',
          handler: async function (data: any): Promise<void> {
            return resolveFunction(data)

          }
        }
      ]
    });
    await prompt.present();
    promise.then(
      result => this.fireAutomatoWithTriggerPoint(result),
      error => alert(error) // doesn't run
    )
  }
  async fireAutomatoWithTriggerPoint(data: any) {
    await this.show();

    var dict = {};

    await AppgainPlugin.fireAutomator(data['triggerPoint'], dict).then(
      result => {
        this.hide();
        this.showAlert(result)
      },
      error => {
        this.hide();
        alert(error)
      } // doesn't run
    )
  }

  async addPurchase() {

    let resolveFunction: (confirm: any) => void;
    const promise = new Promise<any>(resolve => {
      resolveFunction = resolve;
    });
    let prompt = await this.alertController.create({
      header: 'Add purchase',
      inputs: [
        {
          name: 'name',
          placeholder: 'product name'
        },
        {
          name: 'value',
          placeholder: 'amount'
        },
        {
          name: 'currency',
          placeholder: 'currency'
        },
      ],
      buttons: [
        { text: 'Cancel' },
        {
          text: 'OK',
          handler: async function (data: any): Promise<void> {
            return resolveFunction(data)

            console.log(data);

          }
        }
      ]
    });
    await prompt.present();
    promise.then(
      result => this.addpurshace(result),
      error => alert(error) // doesn't run
    )
  }

  async addpurshace(data: any) {
    await this.show();

    await AppgainPlugin.addPurchase(data['name'], data['value'], data['currency']).then(
      result => {
        this.hide();
        this.showAlert(result)
      },
      error => {
        this.hide();
        alert(error)
      } // doesn't run
    )
  }

  async addemailChannel() {

    let resolveFunction: (confirm: any) => void;
    const promise = new Promise<any>(resolve => {
      resolveFunction = resolve;
    });
    let prompt = await this.alertController.create({
      header: 'create notifcation channel',
      inputs: [
        {
          name: 'email',
          placeholder: 'enter email'
        },

      ],
      buttons: [
        { text: 'Cancel' },
        {
          text: 'OK',
          handler: async function (data: any): Promise<void> {
            return resolveFunction(data)
          }
        }
      ]
    });
    await prompt.present();
    promise.then(
      data => this.addnotificationChannel('email', data['email']),
      error => alert(error) // doesn't run
    )
  }

  async addSMSChannel() {
    let resolveFunction: (confirm: any) => void;
    const promise = new Promise<any>(resolve => {
      resolveFunction = resolve;
    });
    let prompt = await this.alertController.create({
      header: 'create notifcation channel',
      inputs: [
        {
          name: 'phone',
          placeholder: 'enter mobile number'
        },

      ],
      buttons: [
        { text: 'Cancel' },
        {
          text: 'OK',
          handler: async function (data: any): Promise<void> {
            return resolveFunction(data)

          }
        }
      ]
    });
    await prompt.present();
    promise.then(
      data => this.addnotificationChannel('SMS', data['phone']),
      error => alert(error) // doesn't run
    )
  }

  async addnotificationChannel(type: string, value: string) {
    await this.show();

    await AppgainPlugin.addNotificationChannel(type, value).then(
      result => {
        this.hide();
        this.showAlert(result)
      },
      error => {
        this.hide();
        alert(error)
      } // doesn't run
    )
  }

  async updateuser() {
    let resolveFunction: (confirm: any) => void;
    const promise = new Promise<any>(resolve => {
      resolveFunction = resolve;
    });
    let prompt = await this.alertController.create({
      header: 'update user',
      inputs: [
        {
          name: 'email',
          placeholder: 'email'
        },
        {
          name: 'phone',
          placeholder: 'phone'
        },
        {
          name: 'f1',
          placeholder: 'field one'
        },
        {
          name: 'f2',
          placeholder: 'field two'
        },
      ],
      buttons: [
        { text: 'Cancel' },
        {
          text: 'OK',
          handler: async function (data: any): Promise<void> {
            return resolveFunction(data)
          }
        }
      ]
    });
    await prompt.present();

    promise.then(
      data => this.updateUserr(data),
      error => alert(error) // doesn't run
    )
  }

  async updateUserr(data: any) {
    await this.show();

    console.log(data);
    var dict = {};
    dict['userEmail'] = data['email'];
    dict['phone'] = data['phone'];
    dict['updateField1'] = data['f1'];
    dict['updateField2'] = data['f2'];
    await AppgainPlugin.updateUserData(dict).then(
      result => {
        this.hide();
        this.showAlert(result)
      },
      error => {
        this.hide();
        alert(error)
      } // doesn't run
    )
  }

  async logevent() {
    let resolveFunction: (confirm: any) => void;
    const promise = new Promise<any>(resolve => {
      resolveFunction = resolve;
    });
    let prompt = await this.alertController.create({
      header: 'log event',
      inputs: [
        {
          name: 'type',
          placeholder: 'Type'
        },
        {
          name: 'action',
          placeholder: 'action'
        },
        {
          name: 'key',
          placeholder: 'key'
        },
        {
          name: 'value',
          placeholder: 'value'
        },
      ],
      buttons: [
        { text: 'Cancel' },
        {
          text: 'OK',
          handler: async function (data: any): Promise<void> {
            return resolveFunction(data)


          }
        }
      ]
    });
    await prompt.present();
    promise.then(
      data => this.logeventt(data),
      error => alert(error) // doesn't run
    )
  }

  async logeventt(data: any) {
    await this.show();
    console.log(data);
    var dict = {};
    dict[data['key']] = data['value'];
    await AppgainPlugin.logEvent(data['type'], data['action'], dict).then(
      result => {
        this.hide();
        this.showAlert(result)
      },
      error => {
        this.hide();
        alert(error)
      } // doesn't run
    )
  }
  async showAlert(message: string) {

    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: '',
      subHeader: '',
      message: 'success',
      buttons: ['OK']
    });

    await alert.present();
  }
  async showAlertt(message: string) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: '',
      subHeader: '',
      message: message,//'success',
      buttons: ['OK']
    });

    await alert.present();
  }
}
